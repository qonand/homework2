<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    /**
     * {@inheritdoc}
     */
    protected $connection = 'mongodb';

    /**
     * {@inheritdoc}
     */
    protected $collection = 'documents';

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
