<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class Client extends Model
{
    use HasFactory, HybridRelations;

    /**
     * {@inheritdoc }
     */
    protected $connection = 'mysql';

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
