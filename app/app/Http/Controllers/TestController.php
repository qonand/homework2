<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Document;
use Elasticsearch;
use PhpParser\Comment\Doc;
use function PHPUnit\Framework\returnArgument;

class TestController extends Controller
{

    /**
     * @var string
     */
    private const INDEX_NAME = 'document';

    /**
     * @return array
     */
    public function run(): array
    {
        $min = Client::min('id');
        $max = Client::max('id');

        $items = [];
        for ($i = 1; $i <= 10; $i++) {
            $items[] = random_int($min, $max);
        }

        $documentIds = [] ;
        foreach ($items as $item) {
            $documentIds = array_merge($documentIds, $this->searchDocuments($item));
        }

        $documents = Document::find($documentIds);
        $documents->load('client');

        return [
            'status' => 'success',
            'data' => $documents,
        ];
    }

    /**
     * @param string $search
     * @return array
     */
    private function searchDocuments(string $search): array
    {
        $parameters = [
            'index' => static::INDEX_NAME,
            'body' => [
                'query' => [
                    'multi_match' => [
                        'query' => $search,
                        'fields' => ['client_id'],
                    ],
                ]
            ]
        ];

        $result = Elasticsearch::search($parameters);

        $documentIds = [];
        foreach ($result['hits']['hits'] as $item) {
            $documentIds[] = $item['_id'];
        }

        return $documentIds;
    }
}
