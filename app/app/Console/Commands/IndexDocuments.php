<?php

namespace App\Console\Commands;

use Elasticsearch;
use App\Models\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class IndexDocuments extends Command
{
    /**
     * @var string
     */
    private const INDEX_NAME = 'document';

    /**
     * {@inheritdoc}
     */
    protected $signature = 'index:documents';

    /**
     * {@inheritdoc }
     */
    protected $description = 'Index documents to elasticsearch';

    /**
     * {@inheritdoc}
     */
    public function handle(): int
    {
        $hasIndex = Elasticsearch::indices()->exists([
            'index' => static::INDEX_NAME
        ]);
        if ($hasIndex) {
            Elasticsearch::indices()->delete([
                'index' => static::INDEX_NAME
            ]);
        }

        Document::chunk(100, function (Collection $documents) {
            foreach ($documents as $document) {

                Elasticsearch::index([
                    'index' => static::INDEX_NAME,
                    'id' => $document->id,
                    'body' => $this->prepareData($document),
                ]);
            }
        });

        return static::SUCCESS;
    }

    /**
     * @param Document $document
     * @return array
     */
    private function prepareData(Document $document): array
    {
        return [
            'client_id' => $document->client_id,
        ];
    }
}
