<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Document;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * @var int
     */
    private const CLIENTS_AMOUNT = 1000;

    /**
     * @var int
     */
    private const DOCUMENTS_AMOUNT_PER_CLIENT = 2;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Client::factory(static::CLIENTS_AMOUNT)->create()->each(static function (Client $client) {
            $documents = Document::factory(static::DOCUMENTS_AMOUNT_PER_CLIENT)->make();
            $client->documents()->saveMany($documents);
        });
    }
}
