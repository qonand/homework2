<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class DocumentFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'client_id' => 1,
            'series' => $this->faker->lexify('??'),
            'number' => $this->faker->numberBetween(),
            'county' => $this->faker->countryCode,
            'address' => $this->faker->address,
        ];
    }
}
