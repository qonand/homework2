# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework2.git`
2. Run `docker-compose up -d nginx mysql elasticsearch mongo workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`
 - `php artisan migrate`
 - `php artisan db:seed` (can take time)
 - `php artisan index:documents` (can take time)

# How to run the application
1. Run `docker-compose up -d nginx mysql elasticsearch mongo workspace` in `laradock` folder
2. Open `http://localhost/api/test/run` in your web browser

# How to run TIG
1. Run `./run.sh` in `docker-influxdb-grafana` folder
2. Open grafana in your brouser `http://localhost:3003/`

# Result of load testing
![Scheme](example.png)